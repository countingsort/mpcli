all: mpcli

mpcli: main.o
	gcc main.o -o mpcli

main.o: main.c
	gcc -c main.c

clean:
	rm *o mpcli
